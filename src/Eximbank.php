<?php
namespace Dayone\Issuer;

class Eximbank {

    public function __construct(){

    }

    public function view()
    {
        \App::register('Dayone\Issuer\EximbankServiceProvider');
        return 'Eximbank::index';
    }

}